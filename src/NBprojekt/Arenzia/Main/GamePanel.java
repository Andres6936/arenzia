/**
 * This file is part of Arenzia.
 * <p>
 * Arenzia is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.**
 * <p>
 * Arenzia is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with Arenzia.
 * If not, see <http://www.gnu.org/licenses/>.
 * Paketname
 */

/** Paketname */
package NBprojekt.Arenzia.Main;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import javax.swing.JPanel;

import NBprojekt.Arenzia.GameState.GameStateManager;


// Erweitern der Klasse 'JPanel', ausführbar machen und Tastatur auslesen hinzufügen
@SuppressWarnings( "serial" )
public class GamePanel extends JPanel implements Runnable, KeyListener
{
    /* Variablendeklaration */

    public static final short WIDTH = 1280;
    public static final short HEIGHT = 800;

    private Thread gameThread;
    private boolean isRunning;
    private static int FPS = 0;
    private static int fps = 130;
    private long targetTime = 1000 / fps;

    private BufferedImage bufferedImage;
    private Graphics2D graphics;

    private GameStateManager gameStateManager;

    /* Standartkonstruktor */

    public GamePanel( )
    {
        super( );
        setPreferredSize( new Dimension( WIDTH, HEIGHT ) );
        setFocusable( true );
        requestFocus( );
    }

    // Initialisierungs-Methode
    public void init( )
    {
        bufferedImage = new BufferedImage( WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB );
        graphics = ( Graphics2D ) bufferedImage.getGraphics( );

        isRunning = true;

        gameStateManager = new GameStateManager( );
    }

    public static int getFPS( )
    {
        return FPS;
    }

    // Ausfuehr-Methode mit der Spielschleife
    public void run( )
    {
        init( );

        long start, departed, period, lastFpsTime = 0, framesLength = 0;
        long lastLoopTime = System.nanoTime( );

        do
        {
            start = System.nanoTime( );
            long now = System.nanoTime( );
            long updateLength = now - lastLoopTime;
            lastLoopTime = now;
            // update the frame counter
            lastFpsTime += updateLength;
            framesLength++;

            if ( lastFpsTime >= 1000000000 )
            {
                lastFpsTime = 0;
                FPS = ( int ) framesLength;
                framesLength -= framesLength;
            }

            update( );
            draw( );
            drawToScreen( );
            departed = System.nanoTime( ) - start;
            period = targetTime + departed / 1000000;

            try
            {
                Thread.sleep( period );
            }
            catch ( Exception e )
            {
                e.printStackTrace( );
            }
        } while ( isRunning );
    }


    // Meldung hinzufuegen
    public void addNotify( )
    {
        super.addNotify( );
        if ( gameThread == null )
        {
            gameThread = new Thread( this );
            addKeyListener( this );
            gameThread.start( );
        }
    }

    /* Methoden fuer das Tastatur mitlesen */

    public void keyTyped( KeyEvent key ) {}

    public void keyPressed( KeyEvent key )
    {
        gameStateManager.keyPressed( key.getKeyCode( ) );
    }

    public void keyReleased( KeyEvent key )
    {
        gameStateManager.keyReleased( key.getKeyCode( ) );
    }


    /* Aktualisierungs- und Zeichnugs-Methoden */

    public void update( )
    {
        gameStateManager.update( );
    }

    public void draw( )
    {
        gameStateManager.draw( graphics );
    }

    public void drawToScreen( )
    {
        Graphics g = getGraphics( );
        g.drawImage( bufferedImage, 0, 0, WIDTH, HEIGHT, null );
        g.dispose( );
    }
}
