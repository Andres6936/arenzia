package NBprojekt.Arenzia.Main;

import java.util.Random;

public class RandomString
{

    public static String randomString( int length )
    {
        StringBuilder randomText = new StringBuilder( );
        int minLength = 10;
        if ( length < minLength )
        { length = minLength; }
        String stringList = "abcdefghijklmnopqrstuvwxyz"
                + "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                + "0123456789+#-*!:;_<>|§$%&/";

        for ( int i = 0; i < length; i++ )
        {
            randomText.append( stringList.charAt( new Random( ).nextInt( stringList.length( ) ) ) );
        }
        return randomText.toString( );
    }
}
