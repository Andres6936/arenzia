/*
  This file is part of Arenzia.
  <p>
  Arenzia is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.**
  <p>
  Arenzia is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  <p>
  You should have received a copy of the GNU General Public License
  along with Arenzia.
  If not, see <http://www.gnu.org/licenses/>.
  Paketname
 */

package NBprojekt.Arenzia.TileMap;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import javax.imageio.ImageIO;

import NBprojekt.Arenzia.Main.GamePanel;


public class Map
{
    public final short TILE_SIZE;

    private final short NUM_DRAW_ROWS;
    private final short NUM_DRAW_COLS;

    private double x;
    private double y;
    private double xMin, yMin, xMax, yMax;

    private int[][] map;

    private int numRows, numCols;
    private int width, height;

    private int numTiles;
    private Tile[][] tiles;

    private int rowOffset, colOffset;

    /* Standartkonstruktor */

    public Map( final short nTileSize )
    {
        TILE_SIZE = nTileSize;

        NUM_DRAW_ROWS = ( short ) (GamePanel.HEIGHT / TILE_SIZE + 3);
        NUM_DRAW_COLS = ( short ) (GamePanel.WIDTH / TILE_SIZE + 3);
    }

    /* Set- und Get-Methoden*/

    public void setPosition( final double x, final double y )
    {
        float tween = 0.04f;
        this.x += ( x - this.x ) * tween;
        this.y += ( y - this.y ) * tween;
        hotFix( );

        colOffset = ( int ) - this.x / TILE_SIZE;
        rowOffset = ( int ) - this.y / TILE_SIZE;
    }

    public double getX( )
    {
        return x;
    }

    public double getY( )
    {
        return y;
    }

    public int getWidth( )
    {
        return width;
    }

    public int getHeight( )
    {
        return height;
    }

    public int getType( int row, int col )
    {
        int rowCol = map[ row ][ col ];
        int r = rowCol / numTiles;
        int c = rowCol % numTiles;

        return tiles[ r ][ c ].getType( );
    }

    public int getNumRows( )
    {
        return numRows;
    }

    public int getNumCols( )
    {
        return numCols;
    }

    /* Sonstige benoetigte Methoden */

    public void loadTiles( String tilesLink )
    {
        try
        {
            BufferedImage tile = ImageIO.read( getClass( ).getResourceAsStream( tilesLink ) );
            numTiles = tile.getWidth( ) / TILE_SIZE;

            tiles = new Tile[ 2 ][ numTiles ];


            for ( int col = 0; col < numTiles; col++ )
            {
                BufferedImage bufferedImage = tile.getSubimage( col * TILE_SIZE, 0, TILE_SIZE, TILE_SIZE );
                tiles[ 0 ][ col ] = new Tile( bufferedImage, Tile.NORMAL );

                bufferedImage = tile.getSubimage( col * TILE_SIZE, TILE_SIZE, TILE_SIZE, TILE_SIZE );
                tiles[ 1 ][ col ] = new Tile( bufferedImage, Tile.BLOCK );
            }
        }
        catch ( Exception e )
        {
            System.out.println( "Can't load the tiles." );
            e.printStackTrace( );
        }
    }

    public void loadMap( String mapLink )
    {
        try
        {
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader( getClass( ).getResourceAsStream( mapLink ) ) );
            numCols = Integer.parseInt( bufferedReader.readLine( ) );
            numRows = Integer.parseInt( bufferedReader.readLine( ) );

            map = new int[ numRows ][ numCols ];
            width = numCols * TILE_SIZE;
            height = numRows * TILE_SIZE;

            xMin = GamePanel.WIDTH - width;
            yMin = GamePanel.HEIGHT - height;

            xMax = yMax = 0;

            String delims = "\\s+";
            for ( int row = 0; row < numRows; row++ )
            {
                String line = bufferedReader.readLine( );
                String[] tokens = line.split( delims );

                for ( int col = 0; col < numCols; col++ )
                {
                    map[ row ][ col ] = Integer.parseInt( tokens[ col ] );
                }
            }
        }
        catch ( Exception e )
        {
            System.out.println( "Can't load the map." );
            e.printStackTrace( );
        }
    }

    private void hotFix( )
    {
        if ( x < xMin )
        { x = xMin; }
        if ( x > xMax )
        { x = xMax; }

        if ( y < yMin )
        { y = yMin; }
        if ( y > yMax )
        { y = yMax; }
    }

    /* Aktualisierungs- und Zeichnugs-Methoden */

    public void draw( Graphics2D graphics )
    {
        for ( int row = rowOffset; row < rowOffset + NUM_DRAW_ROWS; row++ )
        {
            if ( row >= numRows )
            { break; }
            for ( int col = colOffset; col < colOffset + NUM_DRAW_COLS; col++ )
            {
                if ( col >= numCols )
                { break; }

                if ( map[ row ][ col ] == 0 )
                { continue; }

                int rowCol = map[ row ][ col ];
                int r = rowCol / numTiles;
                int c = rowCol % numTiles;
                graphics.drawImage( tiles[ r ][ c ].getBufferedImage( ),
                                    ( int ) x + col * TILE_SIZE, ( int ) y + row * TILE_SIZE, null );
            }
        }
    }
} // End of Map